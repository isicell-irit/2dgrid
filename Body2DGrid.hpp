#ifndef BODY2DGRID_HPP
#define BODY2DGRID_HPP

/**
 * @file Body2DGrid.hpp
 * @brief Defines the Body2DGrid class for 2D grid physics-based bodies.
 *
 * This file contains the Body2DGrid class that works with the 2DGrid plugin.
 */

#include <mecacell/mecacell.h>
#include <mecacell/movable.h>
#include "CellGrid.hpp"
#include "../../../../src/core/BaseBody.hpp"
#define MOVABLE

/**
 * @namespace Grid2D
 * @brief Namespace for 2D grid-related structures and classes.
 */
namespace Grid2D {

    /**
     * @class Body2DGrid
     * @brief Template class for a 2D grid physics-based body.
     * 
     * @tparam cell_t Type of the cell used in the grid.
     * @tparam plugin_t Type of the plugin used with the body.
     */
    template<typename cell_t, class plugin_t>
    class Body2DGrid : public MecaCell::Movable, public virtual BaseBody<plugin_t> {
    private:
        double radius = MecaCell::Config::DEFAULT_CELL_RADIUS;

        Vec2D position2D = Vec2D(0, 0); /**< Position in the grid */
        int nbNeighbouringLocations = 9; /**< Number of neighbouring locations, includes the cell location */
        int nbNeighbouringCells = 1; /**< Number of neighbouring cells */

        CellGrid<cell_t> *cellGrid = nullptr; /**< Pointer to the grid containing all the cells */

        MecaCell::Vector3D position;

    public:

        /**
         * @brief Default constructor.
         */
        inline explicit Body2DGrid() = default;

        /**
         * @brief Constructor with position.
         * @param pos Initial position of the cell in 3D space
         *
         * Sets the position of the cell in the 3D space and in the 2D grid.
         */
        inline explicit Body2DGrid(const MecaCell::Vector3D &pos){
            set2DPosition(Vec2D(pos.x(), pos.y()));
        }

        /**
         * @brief Gets the number of neighbouring locations.
         * @return Number of neighbouring locations
         */
        inline int getNbNeighbouringLocations() const { return nbNeighbouringLocations; }

        /**
         * @brief Sets the number of neighbouring locations.
         * @param n Number of neighbouring locations
         */
        inline void setNbNeighbouringLocations (int n) { nbNeighbouringLocations = n; }

        /**
         * @brief Gets the number of neighbouring cells.
         * @return Number of neighbouring cells
         */
        inline int getNbNeighbouringCells() const { return nbNeighbouringCells; }

        /**
         * @brief Sets the number of neighbouring cells.
         * @param n Number of neighbouring cells
         */
        inline void setNbNeighbouringCells (int n) { nbNeighbouringCells = n; }

        /**
         * @brief Gets the density of the cell.
         * @return Density of the cell
         *
         * Counts the cell itself in the density.
         */
        inline double getDensity() const { return (double)(nbNeighbouringCells+1) / (double)nbNeighbouringLocations; }

        /**
         * @brief Gets the bounding box radius.
         * @return Bounding box radius
         */
        inline double getBoundingBoxRadius() const { return radius; }

        /**
         * @brief Sets the bounding box radius.
         * @param _radius Bounding box radius
         */
        inline void setRadius(double _radius) { radius = _radius; }

        /**
         * @brief Gets the 2D position of the cell.
         * @return 2D position
         */
        inline Vec2D get2DPosition() const { return position2D; }

        /**
         * @brief Gets the 3D position of the cell.
         * @return 3D position
         *
         * Returns the 3D position based on the 2D position with z = 0.
         */
        inline MecaCell::Vector3D getPosition() const { return MecaCell::Vector3D(position2D.x(), position2D.y(), 0.); }

        /**
         * @brief Sets the 2D position of the cell.
         * @param p 2D position
         */
        inline void set2DPosition(const Vec2D &p) {
            int i = p.x();
            int j = p.y();

            int x = position2D.x();
            int y = position2D.y();

            position2D = p;
            position = MecaCell::Vector3D(i, j, 0.);

            if(this->cellPlugin != nullptr){
                auto cellGrid = getCellGrid();
                int c = 0;
                int nbCellSamePosition = cellGrid->grid[x][y].size();
                while(c < nbCellSamePosition && &(cellGrid->grid[x][y][c]->getBody()) != this) c++;
                if(c < nbCellSamePosition){
                    cellGrid->grid[i][j].push_back(cellGrid->grid[x][y][c]);
                    cellGrid->grid[x][y].erase(cellGrid->grid[x][y].begin() + c);
                }
            }
        }

        /**
         * @brief Sets the 2D position of the cell.
         * @param i X-coordinate
         * @param j Y-coordinate
         */
        inline void set2DPosition(int i, int j){ set2DPosition(Vec2D(i, j)); }

        /**
         * @brief Sets the 3D position of the cell.
         * @param p 3D position
         */
        inline void setPosition(const MecaCell::Vector3D &p) { set2DPosition(Vec2D((int)p.x(), (int)p.y())); }

        /**
         * @brief Gets the cell grid.
         * @return Pointer to the cell grid
         */
        inline CellGrid<cell_t> * getCellGrid() { return this->cellPlugin->plugin2DGrid.getCellGrid(); }

        /**
         * @brief Finds one of the lowest density positions within a range. if there are multiple positions with the same lowest density, one is chosen randomly.
         * @param range Range to search for the lowest density position
         * @return 2D position with the lowest density
         */
        Vec2D findLowestDensityPosition(int range) {

            std::vector<int> posibilityX;
            std::vector<int> posibilityY;
            int x = position2D.x();
            int y = position2D.y();
            auto cellGrid = getCellGrid();

            //find min density in neighbour positions
            int min = std::numeric_limits<int>::max();
            int currentSize;
            for (int i = -range; i <= range; ++i) {
                for (int j = -range; j <= range; ++j) {
                    int xi = x + i;
                    int yj = y + j;

                    if (cellGrid->toreX){
                        if(xi < 0) xi = cellGrid->width - xi;
                        if(xi >= cellGrid->width) xi = xi - cellGrid->width;
                    }
                    if(cellGrid->toreY){
                        if(yj < 0) yj = cellGrid->height - yj;
                        if(yj >= cellGrid->height) yj = yj - cellGrid->height;
                    }

                    if ((xi >= 0) && (xi < cellGrid->width) && (yj >= 0) && (yj < cellGrid->height)) {
                        currentSize = cellGrid->grid[xi][yj].size();
                        if (currentSize < min) {
                            min = currentSize;
                            posibilityX.clear();
                            posibilityY.clear();
                            posibilityX.push_back(xi);
                            posibilityY.push_back(yj);
                        } else if (currentSize == min) {
                            posibilityX.push_back(xi);
                            posibilityY.push_back(yj);
                        }
                    }
                }
            }

            //choose the position
            if (posibilityX.size() > 1) {
                std::uniform_int_distribution<> dis(0, posibilityX.size() - 1);
                int choice = dis(MecaCell::Config::globalRand());
                x = posibilityX[choice];
                y = posibilityY[choice];
            } else {
                x = posibilityX[0];
                y = posibilityY[0];
            }
            return Vec2D(x, y);
        }
    };
}
#endif
