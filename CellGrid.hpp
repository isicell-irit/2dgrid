#ifndef GRID2D_HPP
#define GRID2D_HPP

/**
 * @namespace Grid2D
 * @brief Namespace for 2D grid-related structures and classes.
 */
namespace Grid2D {

    /**
    * @struct Vec2D
    * @brief Structure representing a 2D vector.
    */
    struct Vec2D {
        int xc = 0; /**< X-coordinate */
        int yc = 0; /**< Y-coordinate */

        /**
         * @brief Constructor to initialize the vector with given coordinates.
         * @param x_ X-coordinate
         * @param y_ Y-coordinate
         */
        Vec2D(int x_, int y_) : xc(x_), yc(y_) {}

        /**
         * @brief Get the X-coordinate.
         * @return X-coordinate
         */
        inline int x() const { return xc; }

        /**
         * @brief Get the Y-coordinate.
         * @return Y-coordinate
         */
        inline int y() const { return yc; }

        /**
         * @brief Equality operator to compare two Vec2D objects.
         * @param v Another Vec2D object
         * @return True if both vectors have the same coordinates, false otherwise.
         */
        bool operator==(Vec2D v){ return(v.x() == this->x() && v.y() == this->y()); }
    };

    /**
    * @class CellGrid
    * @brief Template class representing a 2D grid of cell pointers.
    * 
    * @tparam cell_t Type of the cell stored in the grid.
    */
    template<typename cell_t>
    struct CellGrid {
        vector<vector<vector<cell_t *>>> grid; /**< 3D vector containing vectors of cell pointers */

        bool toreX = false; /**< Boolean property indicating if the grid wraps around on the X-axis */
        bool toreY = false; /**< Boolean property indicating if the grid wraps around on the Y-axis */
        int height = 10; /**< Height of the grid */
        int width = 10; /**< Width of the grid */

        /**
         * @brief Set the grid size to a square of given size.
         * @param size Size of the grid (both width and height)
         */
        void resizeGrid(int size) {
            resizeGrid(size, size);
        }

        /**
         * @brief Set the grid size to given width and height.
         * @param w Width of the grid
         * @param h Height of the grid
         */
        void resizeGrid(int w, int h) {
            width = w;
            height = h;
            grid.resize(w, vector<vector<cell_t *>>(h, vector<cell_t *>()));
            for(int i = 0 ; i < w ; ++i) grid[i].resize(h, vector<cell_t *>());
        }

        /**
         * @brief Set the toreX property.
         * @param b Boolean value to set toreX
         */
        inline void setToreX(bool b){ toreX = b; }

        /**
         * @brief Set the toreY property.
         * @param b Boolean value to set toreY
         */
        inline void setToreY(bool b){ toreY = b; }

        /**
         * @brief Set both toreX and toreY properties.
         * @param x Boolean value to set toreX
         * @param y Boolean value to set toreY
         */
        inline void setTore(bool x, bool y){ toreX = x; toreY = y; }

        /**
        * @brief Default constructor initializing the grid with default width and height.
        */
        CellGrid() : grid(){
            grid.resize(width, std::vector<std::vector<cell_t *>>(height, std::vector<cell_t *>()));
        }
    };
}

#endif