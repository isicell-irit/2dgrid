#ifndef PLUGIN2DGRID_HPP
#define PLUGIN2DGRID_HPP

/**
 * @file Plugin2DGrid.hpp
 * @brief Defines the Plugin2DGrid class for 2D grid physics-based plugins.
 *
 * This file contains the Plugin2DGrid class that works with the 2DGrid body.
 */

#include <vector>
#include <math.h>
#include "Body2DGrid.hpp"

/**
 * @namespace Grid2D
 * @brief Namespace for 2D grid-related structures and classes.
 */
namespace Grid2D {

    /**
     * @class Plugin2DGrid
     * @brief Template class for a 2D grid physics-based plugin.
     * 
     * @tparam cell_t Type of the cell used in the grid.
     */
    template<typename cell_t>
    class Plugin2DGrid {

    private:
        double nbOccupiedPositions = 0; /**< Number of occupied locations */

        CellGrid<cell_t> cellGrid; /**< Grid containing the cells */

        /**
         * @brief Updates the number of neighbouring locations for a cell.
         * @param c Pointer to the cell
         * 
         * Sets the number of locations based on the Moore neighbourhood, taking toricity into account.
         */
        void updateNeighbouringLocation(cell_t *c) {

            int x = c->getBody().get2DPosition().x();
            int y = c->getBody().get2DPosition().y();

            if(!cellGrid.toreX && !cellGrid.toreY){
                if ((x == 0 || x == cellGrid.width - 1) && (y == 0 || y == cellGrid.height - 1)) {
                    c->getBody().setNbNeighbouringLocations(4);
                } else if (x == 0 || x == cellGrid.width - 1 || y == 0 || y == cellGrid.height - 1) {
                    c->getBody().setNbNeighbouringLocations(6);
                } else {
                    c->getBody().setNbNeighbouringLocations(9);
                }
            }else{
                c->getBody().setNbNeighbouringLocations(9);
            }
        }

        /**
         * @brief Computes the neighbouring cells for a given cell.
         * @param c Pointer to the cell
         */
        void computeNeighbouringCells(cell_t * c) {
            updateNeighbouringLocation(c);
            int x = c->getBody().get2DPosition().x();
            int y = c->getBody().get2DPosition().y();
            c->clearConnectedCells();
            for (int i = -1; i <= 1; ++i) {
                for (int j = -1; j <= 1; ++j) {
                    int xi = x + i;
                    int yj = y + j;
                    if ((xi >= 0) && (xi < cellGrid.width) && (yj >= 0) && (yj < cellGrid.height)) {
                        for(auto cell : cellGrid.grid[xi][yj]){
                            if(cell != c) c->addConnectedCell(cell);
                        }
                    }else if (cellGrid.toreX && cellGrid.toreY){
                        if(xi < 0) xi = cellGrid.width -1;
                        if(xi >= cellGrid.width) xi = 0;
                        if(yj < 0) yj = cellGrid.height -1;
                        if(yj >= cellGrid.height) yj = 0;
                        for(auto cell : cellGrid.grid[xi][yj]){
                            if(cell != c) c->addConnectedCell(cell);
                        }
                    }else if(cellGrid.toreX && (yj >= 0) && (yj < cellGrid.height)){
                        if(xi < 0) xi = cellGrid.width -1;
                        if(xi >= cellGrid.width) xi = 0;
                        for(auto cell : cellGrid.grid[xi][yj]){
                            if(cell != c) c->addConnectedCell(cell);
                        }
                    }else if(cellGrid.toreY && (xi >= 0) && (xi < cellGrid.width)){
                        if(yj < 0) yj = cellGrid.height -1;
                        if(yj >= cellGrid.height) yj = 0;
                        for(auto cell : cellGrid.grid[xi][yj]){
                            if(cell != c) c->addConnectedCell(cell);
                        }
                    }
                }
            }
            c->getBody().setNbNeighbouringCells(c->getConnectedCells().size());
        }

    public:
        /**
         * @brief Default constructor.
         */
        Plugin2DGrid() : cellGrid() {};


        /**
         * @brief Resizes the cell grid to the given width and height.
         * @param w Width of the grid
         * @param h Height of the grid
         */
        void resizeCellGrid(int w, int h){
            cellGrid.resizeGrid(w,h);
        }

        /**
         * @brief Resizes the cell grid to a square of the given size.
         * @param size Size of the grid (both width and height)
         */
        void resizeCellGrid(int size){
            resizeCellGrid(size,size);
        }

        /**
         * @brief Sets the toric property on the X-axis.
         * @param b Boolean value to set toreX
         */
        void setToreX(bool b){
            cellGrid.setToreX(b);
        }

        /**
         * @brief Sets the toric property on the Y-axis.
         * @param b Boolean value to set toreY
         */
        void setToreY(bool b){
            cellGrid.setToreY(b);
        }

        /**
         * @brief Sets the toric properties on both X and Y axes.
         * @param x Boolean value to set toreX
         * @param y Boolean value to set toreY
         */
        void setTore(bool x, bool y){  cellGrid.setTore(x,y); }

        /**
         * @brief Gets the number of occupied positions.
         * @return Number of occupied positions
         */
        inline double getNbOccupiedPositions() const{
            return nbOccupiedPositions;
        }

        /**
         * @brief Gets the occupation surface ratio.
         * @return Occupation surface ratio
         * 
         * Returns the ratio of occupied positions to the total number of possible locations.
         */
        inline double getOccupationSurfaceRatio() const{
            return ((double) nbOccupiedPositions)/(cellGrid.width * cellGrid.height);
        }

        /**
         * @brief Gets the global density of the grid.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         * @return Global density
         */
        template<typename world_t>
        inline double getGlobalDensity(world_t *w) const{
            return (double) w->cells.size()/(cellGrid.width * cellGrid.height);
        }

        /**
         * @brief Gets the cell grid.
         * @return Pointer to the cell grid
         */
        inline CellGrid<cell_t> *getCellGrid() { return &cellGrid; }

        /**
         * @brief Hook called when cells is added to the world.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void onAddCell(world_t *w) {
           for (cell_t *c : w->newCells) {
                int x = c->getBody().get2DPosition().x();
                int y = c->getBody().get2DPosition().y();
                cellGrid.grid[x][y].push_back(c);
            }
        }

        /**
         * @brief Hook called before the behavior update of the world.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void preBehaviorUpdate(world_t *w){
            //updatePositions();
            for (cell_t *c : w->cells) {
                computeNeighbouringCells(c);
            }
        }

        /**
         * @brief Hook called before deleting dead cells from the world.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void preDeleteDeadCellsUpdate(world_t *w) {
            for (cell_t *c : w->cells) {
                if (c->isDead()) {
                    int x = c->getBody().get2DPosition().x();
                    int y = c->getBody().get2DPosition().y();
                    for (int i = 0; i < cellGrid.grid[x][y].size(); ++i) {
                        if (cellGrid.grid[x][y][i] == c) {
                            cellGrid.grid[x][y].erase(cellGrid.grid[x][y].begin() + i);
                        }
                    }
                }
            }
        }
    };
}
#endif
