# 2D Grid

This folder contains the body and plugin classes that could be used to simulate a 2D Grid physics applied on cells using *MecaCell*.

It was tested on Debian 10.

## Prerequisites

- work on a 2D *MecaCell* simulation
- have a general body class that will inherit the **Body2DGrid** class
- have a general plugin class or struct  that will use the **Plugin2DGrid** class


## How to use it

- include the folder in your *MecaCell* project
- import the **Body2DGrid.hpp** file in the file containing your simulation general body class
- make your general body class inherit from the **Body2DGrid** class
```cpp 
template <class cell_t> class CellBody : public Body2DGrid
```  
- in your general body class constructor add the following line :
```cpp 
explicit CellBody(cell_t *, MecaCell::Vec pos = MecaCell::Vec::zero())
: Body2DGrid(pos)
```
- import the **Plugin2DGrid.hpp** file in the file containing your general plugin class or struct
- add a **Plugin2DGrid** attribute to your  general plugin class or struct, this class has to be templated by a cell class
```cpp
Plugin2DGrid<cell_t> physicsPlugin;
```
- in the onRegister method, add the new attribute
```cpp
template <typename world_t>  
void onRegister(world_t* w){  
    w->registerPlugin(physicsPlugin);  
}
```
Your simulation will now implement cells that are subjected to a 2D Grid physics

## How to calibrate your simulation

The only parameters you can set is the size of the grid and its toric properties.
You can do it by modifying the parameter from your scenario class before the main loop starts.
This can be done this way :
```cpp
w.cellPlugin.physicsPlugin.resizeGrid(w,h);
w.cellPlugin.physicsPlugin.setTore(false,true);
```
With this method, the grid is set as a w x h cylinder. The x-axis has no toric property unlike the y-axis.